//Libreria Express
var express = require('express');
var app = express();

//Variable para archivo externo
var usersFile = require('./file.json');
//Deaclara la URL base para todo el programa
var URLbase = '/colbbvaapi/v2/';
//Apunta o importa la libreria body parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());

//Puerto por el que esta escuchando la aplicacion
var port = process.env.PORT || 8000;
app.listen(port);
//console.log('Puerto' + port);

//Peticion POST para el Login
app.post(URLbase + 'login',
  function(request, response) {
    var user = request.body.email;
    var pass = request.body.password;
    console.log(request.body.email);
    console.log(request.body.password);
    for (us of usersFile) {
      if (us.email == user) {
        console.log('usuario encontrado');
        if (us.password == pass) {
          console.log('clave encontrada');
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log('Login correcto!!');
          response.send({
            "msg": "Usuario encontrado."
          });
        } else {
          console.log('Incorrecto!!');
          response.send({
            "msg": "No hay loggin disponible."
          });
        }
      }
    }
  }
);

//Funcion que permite escribir en el archivo
function writeUserDataToFile(data) {
  //console.log('Funcion');
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile('./file.json', jsonUserData, 'utf8',
    function(error) { //función manejadora para gestionar errores de escritura
      if (error) {
        console.log(error);
      } else {
        console.log('Datos escritos en file.json');
      }
    })
}

//Peticion POST para el Logout
app.post(URLbase + 'logout',
  function(request, response) {
    var user = request.body.email;
    var pass = request.body.password;
    console.log(request.body.email);
    console.log(request.body.password);
    for (us of usersFile) {
      if (us.email == user) {
        console.log('usuario encontrado');
        if (us.password == pass) {
          console.log('clave encontrada');
          delete us.logged;
          writeUserDataToFile(usersFile);
          console.log('Logout Exitoso!!');
          response.send({
            "msg": "Usuario ha cerrado la sesion."
          });
        } else {
          console.log('Incorrecto!!');
          response.send({
            "msg": "Hay problemas saliendo de la cuenta."
          });
        }
      }
    }
  }
);

//console.log('Hola soy ColBBVAAPI');
//Hacer mas parametros

var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = '/colbbvaapi/v1/';
//Apunta o importa la libreria body parser
var bodyParser = require('body-parser');

app.listen(port);
//Utiliza parasar un json
//app.use(bodyParser.xml());
app.use(bodyParser.json());
//console.log('App escuchando por el puerto '+port+'!!!');

// PUT
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    var encontrado = false;
    for(i = 0; (i < usersFile.length) && !encontrado; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        encontrado = true;
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    if(!encontrado) {
      res.send({"msg" : "Usuario no encontrado.", updateUser});
    }
  });

// DELETE
app.delete(URLbase + 'users/:id',
 function(req, res) {

   const id = req.params.id-1;
   const reg = usersFile[id];

   if(undefined != reg){
     usersFile.splice(id,1);
     res.send(204);
  } else
    res.send(404);
});

/*
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    console.log('id '+ idBuscar);
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        console.log('Actualiza id: ' + usersFile[i].id);
        res.send({"msg" : "Usuario actualizado correctamente.", "id": idBuscar, updateUser});
      }
    }
    res.send({"msg" : "Usuario no encontrado."});
  });
*/

/*
app.post(URLbase + 'users',
  function (req, res) {
    console.log('POST correcto !!');
    console.log(req.body);
    console.log(req.body.first_name);
    var newID = usersFile.length + 1;
    //var jsonID = {};
    var jsonID = req.body;
    jsonID.id = newID;
    console.log(newID);
    usersFile.push(req.body);
    res.send({"msg":"Usuario creado", jsonID});
    console.log(usersFile);
    //res.send({"msg":"Usuario creado", "id":newID});
    //res.sendfile('users.json'); //Deprecate
    //res.sendFile('users.json',{root: _dir});
});
*/

/*
app.get(URLbase + 'users',
  function(peticion, respuesta){
    //console.log('GET con query string');
    //console.log(peticion.query);
    //console.log(peticion.query.id);
    //console.log(peticion.query.country);
    console.log(peticion.query.name);
    //respuesta.send({'msg':'GET con query string'});
    var name = peticion.query.name;
    var pos = nameFile(name);
    console.log(pos);
    respuesta.send(usersFile[pos - 1]);
  }
);

function nameFile(name){
  console.log('Entra Funcion');
  var nx;
  for(nm in usersFile){
    console.log('Entra For');
    if(nm.first_name = name){
      console.log('Entra If');
      nx = nm;
    }
  }
  console.log(nx);
  return nx;
  console.log('Fin Funcion');
}
*/

//Funcion callback
/*
app.get (URLbase + 'users/:id/:id2',
  function(req, res) {
    console.log('GET /colbbvaapi/v1/users/:id');
    //console.log(req.params);
    //console.log(req.headers);
    //res.sendfile('./users.json');
    var pos = req.params.id;
    var pos2 = req.params.id2;
    console.log(pos);
    console.log(pos2);
    //res.send(usersFile[pos - 1]);
    //var respuesta = 'Parametros' + pos + pos2;
    //res.send(respuesta);
    res.send(req.params);
});
*/

/*
app.put('/user',
  function (req, res) {
    console.log('PUT /colbbvaapi/v1');
    res.send({"msg":"Peticion correcta PUT"});
});
*/

/*
app.delete('/user',
  function (req, res) {
    console.log('DELETE /colbbvaapi/v1');
    res.send({"msg":"Peticion correcta DELETE"});
});
*/

/*
app.listen(8000, function() {
  console.log('Example app listening on port 10000!!!!');
});
*/
